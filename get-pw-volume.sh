#!/bin/bash
wpctl get-volume @DEFAULT_AUDIO_SINK@ | sed 's/Volume: \([[:digit:]]\)\.\([[:digit:]]*\)/[\1\2%]/' | sed 's/MUTED/off/'
