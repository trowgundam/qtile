from libqtile.log_utils import logger

from settings import my_qtile_settings
import groups
import keybindings
import layouts as my_layouts

import hooks

import importlib
import socket

hooks.use_me()  # This is purely to get rid of hte lint error, since comments don't work

groups = groups.groups
keys = keybindings.keys
mouse = keybindings.mouse
layouts = my_layouts.layouts
floating_layout = my_layouts.floating_layout

for key in filter(lambda a: not a.startswith("__"), dir(my_qtile_settings)):
    locals()[key] = getattr(my_qtile_settings, key)

try:
    system_name = socket.gethostname().replace("-", "")
    system_config = importlib.import_module(f"systems.{system_name}")
    if not hasattr(system_config, "screens"):
        raise Exception("System config does not include a set of Screens")

    screens = system_config.screens

    if hasattr(system_config, "keys"):
        keys.extend(system_config.keys)
    if hasattr(system_config, "mouse"):
        mouse.extend(system_config.mouse)

    if hasattr(system_config, "groups"):
        groups = system_config.groups

    if hasattr(system_config, "layouts"):
        layouts = system_config.layouts
    if hasattr(system_config, "extra_layouts"):
        layouts.extend(system_config.extra_layouts)
    if hasattr(system_config, "floating_layout"):
        floating_layout = getattr(system_config, "floating_layout")

    if hasattr(system_config, "my_qtile_settings"):
        for key in filter(
            lambda a: not a.startswith("__"), dir(system_config.my_qtile_settings)
        ):
            locals()[key] = getattr(my_qtile_settings, key)

except Exception as ex:
    logger.exception(ex)
    import screens

    screens = screens.screens
