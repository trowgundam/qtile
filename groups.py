from libqtile.config import Group, Match


def GetGroup(idx: int) -> Group | None:
    if (idx > 10):
        return None

    match idx:
        case 10:
            return Group(
                name=str(idx),
                matches=[
                    Match(wm_class="ffxiv_dx11.exe"),
                    Match(wm_class="XIVLauncher.Core"),
                ],
                label="󰊴")
        case _:
            return Group(str(idx))


groups = [GetGroup(idx) for idx in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]]
