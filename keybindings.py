from libqtile.config import Click, Drag, Key, KeyChord
from libqtile.lazy import lazy

from groups import groups
from settings import mod, terminal

keys = [
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "x", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "e", lazy.spawn("dolphin"), desc="Launch file manager"),
    Key([mod], "v", lazy.window.toggle_floating(), desc="Toggle floating"),
    Key([mod], "r", lazy.spawn("rofi -show drun"), desc="Lauch app menu"),

    Key([], "XF86AudioRaiseVolume",
        lazy.spawn("wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 5%+"),
        desc="Raise volume"),
    Key([], "XF86AudioLowerVolume",
        lazy.spawn("wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 5%-"),
        desc="Lower volume"),
    Key([], "XF86AudioMute",
        lazy.spawn("wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"),
        desc="Mute volume"),

    Key([], "XF86MonBrightnessUp",
        lazy.spawn("brightnessctl -d intel_backlight s 5%+"),
        desc="Raise brightness"),
    Key([], "XF86MonBrightnessDown",
        lazy.spawn("brightnessctl -d intel_backlight s 5%-"),
        desc="Lower brightness"),

    KeyChord(
        [mod],
        "q",
        [
            Key([], "q", lazy.shutdown(), desc="Shutdown Qtile"),
            Key([], "r", lazy.restart(), desc="Restart Qtile"),
            Key([], "l", lazy.reload_config(), desc="Reload the config")
        ],
        name="qtile"
    ),

    Key([mod], "space", lazy.layout.swap_main(), desc="Swap with main"),

    Key([mod], "d", lazy.layout.shuffle_down(), desc="Move window down the stack"),
    Key([mod], "u", lazy.layout.shuffle_up(), desc="Move window up the stack"),
    Key([mod], "l", lazy.layout.grow_main(), desc="Grow Main Window"),
    Key([mod], "h", lazy.layout.shrink_main(), desc="Shrink Main Window"),
    Key([mod], "j", lazy.layout.next(), desc="Focus next window"),
    Key([mod], "k", lazy.layout.previous(), desc="Focus previous window"),
    #
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
]

for idx, g in enumerate(groups):
    key = str(idx+1)
    if idx == 9:
        key = "0"

    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                key,
                lazy.group[g.name].toscreen(),
                desc="Switch to group {}".format(g.name),
            ),
            # mod1 + shift + letter of group =
            #   switch to & move focused window to group
            Key(
                [mod, "shift"],
                key,
                lazy.window.togroup(g.name, switch_group=True),
                desc="Switch to & move focused window to group {}"
                .format(g.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )
#
# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]
