from libqtile import layout
from libqtile.config import Match
from settings import catppuccin_theme

layouts = [
    layout.MonadTall(
        align=layout.MonadTall._left,
        border_focus=catppuccin_theme.sapphire.hex,
        border_normal=catppuccin_theme.base.hex,
        border_width=2,
        change_ratio=0.05,
        margin=4,
        max_ratio=0.8,
        min_ratio=0.20,
        name="",
        new_client_position="top",
        ratio=0.6,
        single_border_width=0,
        single_margin=0,
    ),
    layout.Max(name=""),
    layout.TreeTab(
        active_bg=catppuccin_theme.surface0.hex,
        active_fg=catppuccin_theme.text.hex,
        bg_color=catppuccin_theme.base.hex,
        font="FiraCode Nerd Font",
        fontshadow=catppuccin_theme.crust.hex,
        fontsize=12,
        inactive_bg=catppuccin_theme.base.hex,
        inactive_fg=catppuccin_theme.surface1.hex,
        name="󰙅",),
]

floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of
        # an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
