import os
from libqtile import bar, widget
from libqtile.config import Screen
from libqtile.lazy import lazy
from settings import catppuccin_theme


def GetSep(spad=4):
    return widget.Sep(
        foreground=catppuccin_theme.overlay0.hex,
        padding=spad,
        size_percent=70
    )


home = os.path.expanduser('~')

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayout(
                    fontsize=18,
                    foreground=catppuccin_theme.blue.hex,
                    background=catppuccin_theme.surface0.hex,
                    mouse_callbacks={"Button5": lazy.next_layout},
                    padding=5,
                ),
                widget.GroupBox(
                    active=catppuccin_theme.text.hex,
                    disable_drag=True,
                    fontsize=18,
                    hide_unused=False,
                    highlight_color=[
                        catppuccin_theme.surface0.hex,
                        catppuccin_theme.surface1.hex,
                    ],
                    highlight_method="line",
                    inactive=catppuccin_theme.surface1.hex,
                    padding=1,
                    this_current_screen_border=catppuccin_theme.blue.hex,
                    urgent_alert_method="text",
                    urgent_text=catppuccin_theme.red.hex,
                ),
                widget.Chord(
                    chords_colors={
                        "qtile": (catppuccin_theme.green.hex, catppuccin_theme.base.hex),
                        "rofi": (catppuccin_theme.blue.hex, catppuccin_theme.base.hex),
                        "system": (catppuccin_theme.red.hex, catppuccin_theme.base.hex),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.WindowName(),
                widget.CheckUpdates(
                    color_have_updates=catppuccin_theme.green.hex,
                    color_no_updates=catppuccin_theme.surface1.hex,
                    distr="Arch_yay",
                    mouse_callbacks={"Button1": lazy.force_update},
                    no_update_string="No Updates",
                    padding=4,
                    update_interval=3600,
                ),
                widget.TextBox(
                    fontsize=18,
                    text=chr(983161),
                    foreground=catppuccin_theme.peach.hex,
                    padding=4,
                ),
                widget.Battery(
                    foreground=catppuccin_theme.peach.hex,
                    padding=4,
                    format="{percent:2.0%}"
                ),
                widget.TextBox(
                    fontsize=18,
                    text="󰃞",
                    foreground=catppuccin_theme.yellow.hex,
                    padding=4,
                ),
                widget.Backlight(
                    foreground=catppuccin_theme.yellow.hex,
                    backlight_name="intel_backlight",
                    padding=4,
                ),
                widget.TextBox(
                    fontsize=18,
                    text="",
                    foreground=catppuccin_theme.red.hex,
                    padding=4,
                ),
                widget.CPU(
                    foreground=catppuccin_theme.red.hex,
                    format="{load_percent}%",
                    padding=4,
                ),
                widget.TextBox(
                    fontsize=18,
                    text="",
                    foreground=catppuccin_theme.blue.hex,
                    padding=4,
                ),
                widget.Memory(
                    foreground=catppuccin_theme.blue.hex,
                    format="{MemPercent}%",
                    padding=4,
                ),
                widget.TextBox(
                    fontsize=18,
                    text="",
                    foreground=catppuccin_theme.green.hex,
                    padding=4,
                ),
                widget.Volume(
                    foreground=catppuccin_theme.green.hex,
                    get_volume_command=home+'/.config/qtile/get-pw-volume.sh',
                    volume_down_command="wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 5%-",
                    volume_up_command="wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 5%+",
                    padding=4,
                ),
                widget.Systray(
                    padding=4,
                ),
                widget.Clock(
                    background=catppuccin_theme.surface0.hex,
                    format="%I:%M %p",
                    padding=4,
                ),
            ],
            24,
            background=catppuccin_theme.base.hex,
            border_width=[0, 0, 2, 0],
            border_color=[catppuccin_theme.crust.hex, catppuccin_theme.crust.hex,
                          catppuccin_theme.crust.hex, catppuccin_theme.crust.hex],
            margin=[0, 0, 2, 0]
        ),
        wallpaper="/home/jeff/Pictures/arch-black-4k.png",
        wallpaper_mode="fill",
    ),
]
