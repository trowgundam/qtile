import os
import subprocess

from libqtile import hook
from libqtile.log_utils import logger


def use_me():
    pass


@hook.subscribe.startup_once
def autostart():
    result = subprocess.run(
        ['run-parts', '/usr/lib/systemd/user-environment-generators'],
        capture_output=True,
        text=True,
    )
    for line in result.stdout.splitlines():
        delPos = line.find("=")
        varName = line[:delPos]
        varValue = line[delPos+1:]
        if varName in os.environ:
            os.environ[varName] += ":" + varValue
        else:
            os.environ[varName] = varValue

    home = os.path.expanduser('~')
    subprocess.Popen([home + '/.config/qtile/autostart.sh'])


@hook.subscribe.client_managed
def client_managed(window):
    window.group.toscreen()


def has_exe(classes: list) -> bool:
    for c in classes:
        if str(c).endswith("exe"):
            return True
    return False


def contains(classes: list, target: str) -> bool:
    for c in classes:
        if c == target:
            return True

    return False


@hook.subscribe.client_new
def client_new(window):
    classes: list | None = window.get_wm_class()
    if classes is None:
        return

    if window.fullscreen and has_exe(classes):
        window.togroup("10", switch_group=True)

    if contains(classes, "polkit-kde-authentication-agent-1"):
        window.center()
        window.keep_above(True)
        window.move_to_top()
        window.bring_to_front()
