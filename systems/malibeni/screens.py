from libqtile import bar, widget
from libqtile.config import Screen
from libqtile.lazy import lazy
from libqtile.log_utils import logger
import qtile_extras
from qtile_extras.widget.mixins import TooltipMixin
from settings import catppuccin_theme

import psutil


def GetSep(spad=4):
    return widget.Sep(
        foreground=catppuccin_theme.overlay0.hex, padding=spad, size_percent=70
    )


class TooltipCPU(widget.CPU, TooltipMixin):
    def __init__(self, *args, **kwargs):
        widget.CPU.__init__(self, *args, **kwargs)
        TooltipMixin.__init__(self, **kwargs)
        self.add_defaults(TooltipMixin.defaults)
        self.tooltip_text = TooltipCPU.get_core_utilization()

    def poll(self):
        logger.warning("TooltipCPU Polling")
        self.tooltip_text = TooltipCPU.get_core_utilization()
        return widget.CPU.poll(self)

    @staticmethod
    def get_core_utilization():
        psutil.cpu_percent(percpu=True)
        return "\n".join(
            f"Core {idx}: {u}%" for idx, u in enumerate(psutil.cpu_percent(percpu=True))
        )


screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayout(
                    fontsize=18,
                    foreground=catppuccin_theme.surface0.hex,
                    background=catppuccin_theme.sapphire.hex,
                    mouse_callbacks={"Button5": lazy.next_layout},
                ),
                widget.GroupBox(
                    active=catppuccin_theme.text.hex,
                    block_highlight_text_color=catppuccin_theme.blue.hex,
                    disable_drag=True,
                    fontsize=18,
                    hide_unused=False,
                    highlight_color=[
                        catppuccin_theme.surface1.hex,
                        catppuccin_theme.surface1.hex,
                    ],
                    highlight_method="line",
                    inactive=catppuccin_theme.surface2.hex,
                    padding=1,
                    this_current_screen_border=catppuccin_theme.sapphire.hex,
                    this_screen_border=catppuccin_theme.sapphire.hex,
                    urgent_alert_method="text",
                    urgent_text=catppuccin_theme.red.hex,
                ),
                widget.Chord(
                    chords_colors={
                        "qtile": (
                            catppuccin_theme.green.hex,
                            catppuccin_theme.base.hex,
                        ),
                        "rofi": (catppuccin_theme.blue.hex, catppuccin_theme.base.hex),
                        "system": (catppuccin_theme.red.hex, catppuccin_theme.base.hex),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.WindowName(),
                widget.TextBox(
                    fontsize=18,
                    text="",
                    foreground=catppuccin_theme.surface0.hex,
                    background=catppuccin_theme.red.hex,
                ),
                widget.CPU(
                    foreground=catppuccin_theme.surface0.hex,
                    background=catppuccin_theme.red.hex,
                    format="{load_percent}%",
                ),
                widget.TextBox(
                    fontsize=18,
                    text="",
                    foreground=catppuccin_theme.surface0.hex,
                    background=catppuccin_theme.blue.hex,
                ),
                widget.Memory(
                    foreground=catppuccin_theme.surface0.hex,
                    background=catppuccin_theme.blue.hex,
                    format="{MemPercent}%",
                ),
                widget.TextBox(
                    fontsize=18,
                    text="",
                    foreground=catppuccin_theme.surface0.hex,
                    background=catppuccin_theme.green.hex,
                ),
                widget.Volume(
                    foreground=catppuccin_theme.surface0.hex,
                    background=catppuccin_theme.green.hex,
                ),
                qtile_extras.widget.StatusNotifier(
                    background=catppuccin_theme.surface1.hex,
                ),
                widget.Clock(
                    background=catppuccin_theme.surface2.hex, format="%I:%M %p"
                ),
            ],
            24,
            background=catppuccin_theme.surface0.hex,
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # Borders are magenta
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]
        ),
        wallpaper="~/Pictures/arch-black-4k.png",
        wallpaper_mode="fill",
    ),
]
